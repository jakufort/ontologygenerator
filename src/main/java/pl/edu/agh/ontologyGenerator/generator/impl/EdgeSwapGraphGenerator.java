package pl.edu.agh.ontologyGenerator.generator.impl;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.util.Pair;
import org.jgrapht.graph.SimpleDirectedGraph;
import pl.edu.agh.ontologyGenerator.generator.GraphGenerator;
import pl.edu.agh.ontologyGenerator.main.RelationshipEdgeFactory;
import pl.edu.agh.ontologyGenerator.metrics.GraphMetrics;
import pl.edu.agh.ontologyGenerator.metrics.MetricsEnum;
import pl.edu.agh.ontologyGenerator.metrics.MetricsProcessor;
import pl.edu.agh.ontologyGenerator.metrics.UserMetrics;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.Vertex;

import java.util.Optional;
import java.util.Random;

import static org.jgrapht.Graphs.neighborListOf;

/**
 * Created by Kuba Fortunka on 18.02.2017.
 */
public class EdgeSwapGraphGenerator implements GraphGenerator {

    private GraphGenerator randomGraphGenerator = new RandomGraphGenerator();

    @Override
    public DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(int order) {
        return randomGraphGenerator.generate(order);
    }

    @Override
    public DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(UserMetrics properties) {
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> startGraph = randomGraphGenerator.generate(
                properties.getMetrics().get(MetricsEnum.ORDER).intValue());

        GraphMetrics startMetrics = MetricsProcessor.computeMetricsForGraph(startGraph, properties);
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> copyOfGraph = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
        Graphs.addGraph(copyOfGraph, startGraph);
        for (int i=0;i<10000;i++) {
            swapTwoRandomEdges(copyOfGraph);
            GraphMetrics afterSwapMetrics = MetricsProcessor.computeMetricsForGraph(copyOfGraph, properties);
//            System.out.println("afterswapmetrics: " + afterSwapMetrics);
            if (properties.fstGraphIsBetterThanSecond(afterSwapMetrics, startMetrics)) {
                startGraph = copyOfGraph;
                startMetrics = afterSwapMetrics;
            }
        }
        System.out.println("Final metrics: " + startMetrics);
        return startGraph;
    }

    private void swapTwoRandomEdges(DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        for (int i=0;i<100;i++) {
            Optional<Pair<RelationshipEdge<Vertex<String>>, RelationshipEdge<Vertex<String>>>> maybeEdges = get2RandomEdges(graph);
            if (maybeEdges.isPresent()) {
                Pair<RelationshipEdge<Vertex<String>>, RelationshipEdge<Vertex<String>>> edges = maybeEdges.get();
                RelationshipEdge<Vertex<String>> e1 = edges.first;
                RelationshipEdge<Vertex<String>> e2 = edges.second;
                Vertex<String> s1 = e1.getFrom();
                Vertex<String> s2 = e1.getTo();
                Vertex<String> r1 = e2.getFrom();
                Vertex<String> r2 = e2.getTo();
                RelationshipEdge<Vertex<String>> newE1 = new RelationshipEdge<>(s1, r2, e1.getType(), e1.getValue());
                RelationshipEdge<Vertex<String>> newE2 = new RelationshipEdge<>(r1, s2, e2.getType(), e2.getValue());
                if (isLegalEdge(newE1, graph) && isLegalEdge(newE2, graph)) {
                    graph.addEdge(s1, r2, newE1);
                    graph.addEdge(r1, s2, newE2);
                    graph.removeEdge(s1, s2);
                    graph.removeEdge(r1, r2);
                    break;
                }
            }
        }
    }

    private static final Random rnd = new Random();

    private Optional<Pair<RelationshipEdge<Vertex<String>>, RelationshipEdge<Vertex<String>>>> get2RandomEdges(
            DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        RelationshipEdge[] edges = graph.edgeSet().toArray(new RelationshipEdge[graph.edgeSet().size()]);
        int fstEdge = rnd.nextInt(edges.length);
        Vertex r1 = (Vertex) edges[fstEdge].getFrom();
        Vertex r2 = (Vertex) edges[fstEdge].getTo();
        int tries = 0;
        Optional<Pair<RelationshipEdge<Vertex<String>>, RelationshipEdge<Vertex<String>>>> ans = Optional.empty();
        while (!ans.isPresent() && tries < edges.length) {
            int sndEdge = rnd.nextInt(edges.length);
            Vertex s1 = (Vertex) edges[sndEdge].getFrom();
            Vertex s2 = (Vertex) edges[sndEdge].getTo();
            if (!s1.equals(r1) && !s1.equals(r2) && !s2.equals(r1) && !s2.equals(r2)
                    && !neighborListOf(graph, s1).contains(r2) && !neighborListOf(graph, r1).contains(s2)) {
                return Optional.of(new Pair<>(edges[fstEdge], edges[sndEdge]));
            }
            tries++;
        }
        return ans;
    }
}
