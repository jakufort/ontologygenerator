package pl.edu.agh.ontologyGenerator.generator;

import org.jgrapht.DirectedGraph;
import pl.edu.agh.ontologyGenerator.metrics.UserMetrics;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdgeEnum;
import pl.edu.agh.ontologyGenerator.model.Vertex;
import pl.edu.agh.ontologyGenerator.model.VertexType;

import java.security.SecureRandom;

/**
 * Created by Wiktor on 2017-02-11.
 */
public interface GraphGenerator {
    SecureRandom random = new SecureRandom();

    DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(int order);

    DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(UserMetrics properties);

    default <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    default boolean isLegalEdge(RelationshipEdge<Vertex<String>> edge, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        switch(edge.getType()) {
            case SUBCLASS:
                return canBeSuperClass(edge.getFrom(), edge.getTo(), graph);
            case ASSOCIATION:
                return canCreateAssociation(edge.getFrom(), edge.getTo(), graph);
            case PROPERTY:
                return (edge.getFrom().getType() != VertexType.PROPERTY) &&
                        (edge.getTo().getType() != VertexType.PROPERTY || !isAlreadyBound(edge.getTo(), graph));
            default:
                return false;
        }
    }

    default boolean subclassWillCreateCycle(Vertex<String> from, Vertex<String> to, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return to.equals(from) ||
                graph.outgoingEdgesOf(to).stream()
                        .filter(x -> x.getType() == RelationshipEdgeEnum.SUBCLASS)
                        .map(RelationshipEdge::getTo).anyMatch(x -> subclassWillCreateCycle(from, x, graph));
    }

    // no asssociation between v, v1
    // v is not an ancestor of v1
    // v1 is not an ancestor of v
    default boolean canBeSuperClass(Vertex<String> v, Vertex<String> v1, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return !isDirectSubclassOf(v, v1, graph) && !isDirectSubclassOf(v1, v, graph) && !subclassWillCreateCycle(v, v1, graph);
    }

    default boolean canCreateAssociation(Vertex<String> from, Vertex<String> to, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return !(associationExists(from, to, graph) || isDirectRelative(from, to, graph));
    }

    default boolean associationExists(Vertex<String> v, Vertex<String> v1, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return graph.getAllEdges(v, v1).stream().anyMatch(RelationshipEdge::edgeIsAssociation) || graph.getAllEdges(v1, v).stream().anyMatch(RelationshipEdge::edgeIsAssociation);
    }

    default boolean isDirectRelative(Vertex<String> v, Vertex<String> v1, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return (isDirectSubclassOf(v, v1, graph) || isDirectSubclassOf(v1, v, graph));
    }

    // class can be a subclass of only 1 class
    // class can't be a subclass of its own sublass
    default boolean isDirectSubclassOf(Vertex<String> vertex, Vertex<String> of, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return graph.outgoingEdgesOf(of).stream().anyMatch(e -> e.getType() == RelationshipEdgeEnum.SUBCLASS && e.getTo().equals(vertex));
    }

    default boolean isAlreadyBound(Vertex<String> property, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        return graph.incomingEdgesOf(property).size() > 0;
    }

}
