package pl.edu.agh.ontologyGenerator.generator.impl;

import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.SimpleDirectedGraph;
import pl.edu.agh.ontologyGenerator.generator.GraphGenerator;
import pl.edu.agh.ontologyGenerator.main.RelationshipEdgeFactory;
import pl.edu.agh.ontologyGenerator.metrics.GraphMetrics;
import pl.edu.agh.ontologyGenerator.metrics.MetricsEnum;
import pl.edu.agh.ontologyGenerator.metrics.MetricsProcessor;
import pl.edu.agh.ontologyGenerator.metrics.UserMetrics;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdgeEnum;
import pl.edu.agh.ontologyGenerator.model.Vertex;
import pl.edu.agh.ontologyGenerator.model.VertexType;

import java.util.*;

/**
 * Created by Wiktor on 2017-02-11.
 */
public class RandomGraphGenerator implements GraphGenerator {

    private static final int TARGET_INDEX_MAX_LOOPS = 50;

    @Override
    public DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(UserMetrics properties) {
        Number size = properties.getMetrics().get(MetricsEnum.ORDER);
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> ans = generate(size == null ? (random.nextInt(50) + 1) : size.intValue());
        GraphMetrics currentBestMetrics = MetricsProcessor.computeMetricsForGraph(ans, properties);
        final int iterationLimit = properties.getMetrics().get(MetricsEnum.ITERATION_LIMIT) != null ? properties.getMetrics().get(MetricsEnum.ITERATION_LIMIT).intValue() : 20;
        for (int i = 0; i < iterationLimit; i++) {
            DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> contender = generate(size == null ? (random.nextInt(50) + 1) : size.intValue());
            GraphMetrics contenderMetrics = MetricsProcessor.computeMetricsForGraph(contender, properties);
            if (properties.fstGraphIsBetterThanSecond(contenderMetrics, currentBestMetrics)) {
                ans = contender;
                currentBestMetrics = contenderMetrics;
            }
        }
        System.out.println(currentBestMetrics);
        return ans;
    }

    /*
       for every vertex generate random number of edges of random type and look for random vertex to connect to
       limit useless check by simply lowering the max random number over time
    */
    public DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> generate(int order) {
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);

        for (int i = 0; i < order; i++) {
            graph.addVertex(new Vertex<>("Value" + i, randomEnum(VertexType.class)));
        }

        List<Vertex<String>> vertexList = Collections.unmodifiableList(new ArrayList<>(graph.vertexSet()));
        // HAIL COMPLETELY RANDOM CONNECTION COUNT
        int maxConnections = (int) Math.round(0.7 * vertexList.size());
        int maxId = vertexList.size() - 1;

        for (int sourceIndex = 0; sourceIndex < vertexList.size(); sourceIndex++) {
            int noOfConnections = random.nextInt(maxConnections);
            maxConnections = (int) Math.round(0.9 * maxConnections);

            for (int j = 0; j < noOfConnections; j++) {
                int targetIndex, count = 0;
                do {
                    targetIndex = random.nextInt(vertexList.size());
                    count++;
                    if (count > TARGET_INDEX_MAX_LOOPS) break;
                } while (sourceIndex == targetIndex);

                Optional<RelationshipEdgeEnum> edgeEnumOptional = getProperRandomEnum(vertexList.get(sourceIndex), vertexList.get(targetIndex), graph);
                if (edgeEnumOptional.isPresent()) {
                    RelationshipEdge<Vertex<String>> edge = new RelationshipEdge<>(vertexList.get(sourceIndex), vertexList.get(targetIndex),
                            edgeEnumOptional.get(), "conn-" + sourceIndex + "-to-" + targetIndex);
                    graph.addEdge(vertexList.get(sourceIndex), vertexList.get(targetIndex), edge);
                }
            }
        }
        return graph;
    }


    // create SUBCLASS between v, v1 == v1 is SUBCLASS_OF v
    private Optional<RelationshipEdgeEnum> getProperRandomEnum(Vertex<String> from, Vertex<String> to, DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        // if FROM is PROPERTY, then you can't exactly do much
        if (from.getType() == VertexType.PROPERTY) {
            return Optional.empty();
        }

        // if TO is PROPERTY, then you can only go with PROPERTY relationship
        if (to.getType() == VertexType.PROPERTY) {
            if (isAlreadyBound(to, graph)) {
                return Optional.empty();
            } else {
                return Optional.of(RelationshipEdgeEnum.PROPERTY);
            }
        }

        // all that's left are classes
        List<RelationshipEdgeEnum> properEnums = new ArrayList<>(Arrays.asList(RelationshipEdgeEnum.values()));
        if (!canBeSuperClass(from, to, graph)) {
            properEnums.remove(RelationshipEdgeEnum.SUBCLASS);
        }

        // Associations between inheriting classes: no for direct relatives?
        if (!canCreateAssociation(from, to, graph)) {
            properEnums.remove(RelationshipEdgeEnum.ASSOCIATION);
        }

        if (properEnums.isEmpty()) {
            return Optional.empty();
        } else {
            int index = random.nextInt(properEnums.size());
            if (index < 0) {
                index = 0;
            }
            return Optional.of(properEnums.get(index));
        }
    }



}
