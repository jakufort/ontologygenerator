package pl.edu.agh.ontologyGenerator.model;

/**
 * Created by Jakub Fortunka on 19.12.2016.
 */
public enum RelationshipEdgeEnum {
    ASSOCIATION, SUBCLASS, PROPERTY;//, AGGREGATION, COMPOSITION, ASSOCIATIONFROM, ASSOCIATIONTO;
}
