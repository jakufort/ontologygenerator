package pl.edu.agh.ontologyGenerator.model;

/**
 * Created by Jakub Fortunka on 19.12.2016.
 */
public class RelationshipEdge<V> {

    private RelationshipEdgeEnum type;
    private V from;
    private V to;
    private String value;

    public RelationshipEdge(V from, V to, RelationshipEdgeEnum type) {
        this.type = type;
        this.from = from;
        this.to = to;
    }

    public RelationshipEdge(V from, V to, RelationshipEdgeEnum type, String value) {
        this.from = from;
        this.to = to;
        this.type = type;
        this.value = value;
    }

    public V getFrom() {
        return from;
    }

    public V getTo() {
        return to;
    }

    public void setFrom(V from) {
        this.from = from;
    }

    public void setTo(V to) {
        this.to = to;
    }

    public RelationshipEdgeEnum getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return type.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof RelationshipEdge) {
            RelationshipEdge edge = (RelationshipEdge)obj;
            if (edge.getFrom().equals(this.from) && edge.getTo().equals(this.to) && edge.getType() == this.type) {
                return true;
            }
        }
        return false;
    }

    public static <T> boolean edgeIsAssociation(RelationshipEdge<T> e) {
        return e.getType() == RelationshipEdgeEnum.ASSOCIATION;
    }

}
