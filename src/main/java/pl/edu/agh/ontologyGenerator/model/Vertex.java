package pl.edu.agh.ontologyGenerator.model;

/**
 * Created by Jakub Fortunka on 10.01.2017.
 */
public class Vertex<T> {

    private T value;
    private VertexType type;
//    private VertexMetrics metrics;

//    public void computeVertexMetrics(DirectedGraph<Vertex<T>, RelationshipEdge> graphWithVertex) {
//        graphWithVertex.edgesOf(this)
//    }

    public Vertex(T value, VertexType type) {
        this.value = value;
        this.type = type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public VertexType getType() {
        return type;
    }

    public void setType(VertexType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vertex) {
            Vertex v = (Vertex) obj;
            if (v.getValue().equals(this.value) && v.getType() == this.getType()) {
                return true;
            }
        }
        return false;
    }
}
