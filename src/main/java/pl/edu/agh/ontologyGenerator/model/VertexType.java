package pl.edu.agh.ontologyGenerator.model;

/**
 * Created by Jakub Fortunka on 10.01.2017.
 */
public enum VertexType {
    CLASS, PROPERTY;//, OBJECT;
}
