package pl.edu.agh.ontologyGenerator.main;

import org.apache.commons.cli.ParseException;
import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.ExportException;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import pl.edu.agh.ontologyGenerator.generator.GraphGenerator;
import pl.edu.agh.ontologyGenerator.generator.impl.EdgeSwapGraphGenerator;
import pl.edu.agh.ontologyGenerator.metrics.UserMetrics;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdgeEnum;
import pl.edu.agh.ontologyGenerator.model.Vertex;
import pl.edu.agh.ontologyGenerator.model.VertexType;
import pl.edu.agh.ontologyGenerator.util.CmdLineArgumentsHandler;
import pl.edu.agh.ontologyGenerator.util.PropertiesUtil;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;

/**
 * Created by Jakub Fortunka on 12.12.2016.
 */
//TODO ORDER metric have to be mandatory
public class Main {

    private static final String PROPERTIES_FILE = "graph.properties";

    public static void main(String[] args) {
        try {
            Optional<Properties> cliProperties = CmdLineArgumentsHandler.handleCommandLineArguments(args);
            UserMetrics userMetrics = cliProperties.isPresent() ? new UserMetrics(cliProperties.get()) : new UserMetrics(PropertiesUtil.loadExternalProperties(PROPERTIES_FILE));
            GraphGenerator generator = new EdgeSwapGraphGenerator();
            DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> randomGraph = generator.generate(userMetrics);

            GraphConverter.exportGraphToDOT(randomGraph, new File("exported-dot.txt"));
            GraphConverter.saveGraphToOwlFile(randomGraph, new File("randomOwl.owl"));
        } catch (IOException e) {
            System.out.println("No *.properties file was found :(");
            CmdLineArgumentsHandler.showHelp();
        } catch (ParseException | OWLOntologyStorageException | OWLOntologyCreationException | ExportException e) {
            e.printStackTrace();
        }
    }

    private static void createOntGraphManually() throws OWLOntologyCreationException, OWLOntologyStorageException {
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
        Vertex<String> artist = new Vertex<>("Artist", VertexType.CLASS);
        Vertex<String> artefact = new Vertex<>("Artefact", VertexType.CLASS);
        Vertex<String> sculptor = new Vertex<>("Sculptor", VertexType.CLASS);
        Vertex<String> painter = new Vertex<>("Painter", VertexType.CLASS);
        Vertex<String> sculpture = new Vertex<>("Sculpture", VertexType.CLASS);
        Vertex<String> painting = new Vertex<>("Painting", VertexType.CLASS);
        Vertex<String> museum = new Vertex<>("Museum", VertexType.CLASS);
        Vertex<String> firstName = new Vertex<>("firstName", VertexType.PROPERTY);
        Vertex<String> lastName = new Vertex<>("lastName", VertexType.PROPERTY);
        Vertex<String> material = new Vertex<>("material", VertexType.PROPERTY);
        Vertex<String> technique = new Vertex<>("technique", VertexType.PROPERTY);
        Arrays.asList(artist, artefact, sculptor, painter, sculpture, painting, museum, firstName, lastName, material, technique).forEach(graph::addVertex);

        graph.addEdge(artist, sculptor, new RelationshipEdge<>(artist, sculptor, RelationshipEdgeEnum.SUBCLASS, ""));
        graph.addEdge(artist, painter, new RelationshipEdge<>(artist, painter, RelationshipEdgeEnum.SUBCLASS));
        graph.addEdge(artefact, sculpture, new RelationshipEdge<>(artefact, sculpture, RelationshipEdgeEnum.SUBCLASS));
        graph.addEdge(artefact, painting, new RelationshipEdge<>(artefact, painting, RelationshipEdgeEnum.SUBCLASS));
        graph.addEdge(artist, firstName, new RelationshipEdge<>(artist, firstName, RelationshipEdgeEnum.PROPERTY));
        graph.addEdge(artist, lastName, new RelationshipEdge<>(artist, lastName, RelationshipEdgeEnum.PROPERTY));
        graph.addEdge(sculpture, material, new RelationshipEdge<>(sculpture, material, RelationshipEdgeEnum.PROPERTY));
        graph.addEdge(painting, technique, new RelationshipEdge<>(painting, technique, RelationshipEdgeEnum.PROPERTY));
        graph.addEdge(artist, artefact, new RelationshipEdge<>(artist, artefact, RelationshipEdgeEnum.ASSOCIATION, "creates"));
        graph.addEdge(sculptor, sculpture, new RelationshipEdge<>(sculptor, sculpture, RelationshipEdgeEnum.ASSOCIATION, "sculpts"));
        graph.addEdge(painter, painting, new RelationshipEdge<>(painter, painting, RelationshipEdgeEnum.ASSOCIATION, "paints"));
        graph.addEdge(artefact, museum, new RelationshipEdge<>(artefact, museum, RelationshipEdgeEnum.ASSOCIATION, "exhibites"));

        GraphConverter.saveGraphToOwlFile(graph, new File("graphOwl.owl"));
    }


}
