package pl.edu.agh.ontologyGenerator.main;

import org.jgrapht.DirectedGraph;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.ExportException;
import org.jgrapht.ext.VertexNameProvider;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.OWLXMLDocumentFormat;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.util.DefaultPrefixManager;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.Vertex;

import java.io.File;

/**
 * Created by Jakub Fortunka on 15.01.2017.
 */
public class GraphConverter {

    private static final String BASE_IRI = "http://localhost/ontology";

    public static <T> void saveGraphToOwlFile(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph, File f) throws OWLOntologyCreationException, OWLOntologyStorageException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = convertGraphToOntology(graph);
        manager.saveOntology(ontology, new OWLXMLDocumentFormat(), IRI.create(f.toURI()));
    }

    public static <T> OWLOntology convertGraphToOntology(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph) throws OWLOntologyCreationException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.createOntology(IRI.create(BASE_IRI));
        OWLDataFactory factory = manager.getOWLDataFactory();
        PrefixManager pm = new DefaultPrefixManager(null, null, BASE_IRI + "#");
        for (Vertex<T> v : graph.vertexSet()) {
            switch (v.getType()) {
                case CLASS:
                    OWLClass owlClass = getClassFromVertex(v, pm, factory, manager, ontology);
//                    OWLClass owlClass = factory.getOWLClass(":" + v.getValue().toString(), pm);
//                    OWLDeclarationAxiom declarationAxiom = factory.getOWLDeclarationAxiom(owlClass);
//                    manager.addAxiom(ontology, declarationAxiom);
                    for (RelationshipEdge<Vertex<T>> e : graph.outgoingEdgesOf(v)) {
                        switch (e.getType()) {
                            case PROPERTY:
                                OWLDataProperty prop = factory.getOWLDataProperty(v.getValue().toString() + "." + e.getTo().getValue().toString(), pm);
                                OWLDataPropertyRangeAxiom propertyRangeAxiom = factory.getOWLDataPropertyRangeAxiom(prop, OWL2Datatype.XSD_STRING);
//                                factory.getOWLDataPropertyRangeAxiom(prop, OWL2Datatype.XSD_STRING);
                                OWLDataPropertyDomainAxiom domainAxiom = factory.getOWLDataPropertyDomainAxiom(prop, owlClass);
                                manager.addAxiom(ontology, domainAxiom);
                                manager.addAxiom(ontology, propertyRangeAxiom);
                                break;
                            case SUBCLASS:
                                OWLClass childClass = getClassFromVertex(e.getTo(), pm, factory, manager, ontology);
                                OWLSubClassOfAxiom ax = factory.getOWLSubClassOfAxiom(childClass, owlClass);
                                manager.applyChange(new AddAxiom(ontology, ax));
                                break;
                            case ASSOCIATION:
                                OWLObjectProperty objProp = factory.getOWLObjectProperty(v.getValue().toString() + "." + e.getValue(), pm);
                                OWLClass rangeClass = getClassFromVertex(e.getTo(), pm, factory, manager, ontology);
                                OWLObjectPropertyRangeAxiom objectPropertyRangeAxiom = factory.getOWLObjectPropertyRangeAxiom(objProp, rangeClass);
                                OWLObjectPropertyDomainAxiom objectPropertyDomainAxiom = factory.getOWLObjectPropertyDomainAxiom(objProp, owlClass);
                                manager.addAxiom(ontology, objectPropertyDomainAxiom);
                                manager.addAxiom(ontology, objectPropertyRangeAxiom);
                                break;
                        }
                    }
                    break;
            }
        }
        return ontology;
    }

    public static <T> void exportGraphToDOT(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph, File file) throws ExportException {
        VertexNameProvider<Vertex<T>> vertexIdProvider = new VertexNameProvider<Vertex<T>>() {
            @Override
            public String getVertexName(Vertex<T> tVertex) {
                return tVertex.getValue().toString();
            }
        };
        VertexNameProvider<Vertex<T>> vertexLabelProvider = new VertexNameProvider<Vertex<T>>() {
            @Override
            public String getVertexName(Vertex<T> tVertex) {
                return "[" + tVertex.getType().toString() + "] " + tVertex.getValue();
            }
        };
        EdgeNameProvider<RelationshipEdge<Vertex<T>>> enProvider = new EdgeNameProvider<RelationshipEdge<Vertex<T>>>() {
            @Override
            public String getEdgeName(RelationshipEdge<Vertex<T>> vertexRelationshipEdge) {
                return vertexRelationshipEdge.getType().toString();
            }
        };

        DOTExporter<Vertex<T>, RelationshipEdge<Vertex<T>>> dotExporter = new DOTExporter<>(vertexIdProvider, vertexLabelProvider, enProvider);
        dotExporter.exportGraph(graph, file);
    }

    private static <T> OWLClass getClassFromVertex(Vertex<T> vertex, PrefixManager pm, OWLDataFactory factory, OWLOntologyManager manager, OWLOntology ontology) {
        return factory.getOWLClass(vertex.getValue().toString(), pm);
    }
}
