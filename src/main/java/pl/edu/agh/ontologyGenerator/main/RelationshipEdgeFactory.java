package pl.edu.agh.ontologyGenerator.main;

import org.jgrapht.EdgeFactory;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdgeEnum;
import pl.edu.agh.ontologyGenerator.model.Vertex;

/**
 * Created by Jakub Fortunka on 11.01.2017.
 */
public class RelationshipEdgeFactory implements EdgeFactory<Vertex<String>, RelationshipEdge<Vertex<String>>> {

    public static final RelationshipEdgeFactory INSTANCE = new RelationshipEdgeFactory();

    @Override
    public RelationshipEdge<Vertex<String>> createEdge(Vertex<String> v1, Vertex<String> v2) {
        return new RelationshipEdge<>(v1, v2, RelationshipEdgeEnum.PROPERTY, "");
    }

    private RelationshipEdgeFactory() {}

}
