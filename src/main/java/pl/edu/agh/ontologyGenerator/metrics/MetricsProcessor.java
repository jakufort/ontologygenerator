package pl.edu.agh.ontologyGenerator.metrics;

import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.FloydWarshallShortestPaths;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdgeEnum;
import pl.edu.agh.ontologyGenerator.model.Vertex;
import pl.edu.agh.ontologyGenerator.model.VertexType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.jgrapht.Graphs.neighborListOf;

/**
 * Created by Jakub Fortunka on 13.12.2016.
 */
public class MetricsProcessor {

    public static <T> GraphMetrics computeMetricsForGraph(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph, UserMetrics metrics) {
        GraphMetrics<T> graphMetrics = new GraphMetrics<>();
        FloydWarshallShortestPaths<Vertex<T>, RelationshipEdge<Vertex<T>>> floydWarshallShortestPaths = new FloydWarshallShortestPaths<>(graph);
        for (MetricsEnum userMetric : metrics.getMetrics().keySet()) {
            Number value;
            switch (userMetric) {
                case AVG_DEGREE:
                    value = ((double) (graph.vertexSet().stream().map(v -> graph.inDegreeOf(v) + graph.outDegreeOf(v)).reduce(0, (acc, x) -> acc + x))) / (double) graph.vertexSet().size();
                    break;
                case ORDER:
                    value = graph.vertexSet().size();
                    break;
                case SIZE:
                    value = graph.edgeSet().size();
                    break;
                case DIAMETER:
                    value = floydWarshallShortestPaths.getDiameter();
                    break;
                case RADIUS:
                    value = getEccentricities(floydWarshallShortestPaths, graph).stream().min(Integer::compare).orElse(-1);
                    break;
                case ASSORTATIVITY:
//                    SimpleDirectedGraph graphWithoutDatatype = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
//                    graph.vertexSet().stream().filter(v -> v.getType() != VertexType.PROPERTY).forEach(graphWithoutDatatype::addVertex);
//                    graph.edgeSet().stream().filter(e -> e.getType() != RelationshipEdgeEnum.PROPERTY).forEach(e -> graphWithoutDatatype.addEdge(e.getFrom(), e.getTo(), e));
                    value = computeAssortativity(graph);
                    break;
                case LOCAL_CLUSTERING_COEFFICIENT:
//                    SimpleDirectedGraph withoutDatatype = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
//                    graph.vertexSet().stream().filter(v -> v.getType() != VertexType.PROPERTY).forEach(withoutDatatype::addVertex);
//                    graph.edgeSet().stream().filter(e -> e.getType() != RelationshipEdgeEnum.PROPERTY).forEach(e -> withoutDatatype.addEdge(e.getFrom(), e.getTo(), e));
                    value = computerLocalClustering(graph);
                    break;
                default:
                    value = -1;
            }
            graphMetrics.addMetric(userMetric, value);
        }
        graphMetrics.setShortestPaths(floydWarshallShortestPaths);
        return graphMetrics;
    }

    private static <T> Double computeAssortativity(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph) {
        double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0;
        List<RelationshipEdge<Vertex<T>>> interestingEdges = graph.edgeSet().stream().filter(x -> x.getType() != RelationshipEdgeEnum.PROPERTY).collect(Collectors.toList());
        double edgeCount = interestingEdges.size();
        for (RelationshipEdge<Vertex<T>> e : interestingEdges) {
            //TODO don't know how to decide if we want to have inDegreeOf or outDegreeOf
            int srcNodeDegree = graph.inDegreeOf(e.getFrom());
            int dstNodeDegree = graph.inDegreeOf(e.getTo());
            sum1 +=  (srcNodeDegree * dstNodeDegree);
            sum2 +=  (srcNodeDegree + dstNodeDegree);
            sum3 +=  (srcNodeDegree * srcNodeDegree + dstNodeDegree * dstNodeDegree);
        }

        final double sum1m = sum1 / edgeCount;

        double sum2m = sum2 / (2 * edgeCount);
        sum2m *= sum2m;

        final double sum3m = sum3 / (2 * edgeCount);

        final double enumerator = sum1m - sum2m;
        final double denominator = sum3m - sum2m;

        return enumerator == denominator ? 1 : enumerator / denominator;
    }

    public static <T> Number computerLocalClustering(DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph) {
        List<Vertex<T>> interestingVertices = graph.vertexSet().stream().filter(x -> x.getType() == VertexType.CLASS).collect(Collectors.toList());
        return interestingVertices.stream()
                .map(x -> calculateVertex(x, graph))
                .reduce(0.0, (x,acc) -> acc + x) / (double)interestingVertices.size();
    }

    private static <T> double calculateVertex(Vertex<T> vertex, DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph) {
        List<Vertex<T>> neighbours = neighborListOf(graph, vertex);
        if (neighbours.size() < 2) return 0.0;
        int edgesMax = neighbours.size() * (neighbours.size() - 1);
        int edgesActual = 0;
        for (Vertex<T> n1 : neighbours) {
            for (Vertex<T> n2 : neighbours) {
                if (!n1.equals(n2)) {
                    if (graph.containsEdge(n1, n2)) {
                        edgesActual++;
                    }
                }
            }
        }
        return (double)edgesActual / edgesMax;
    }

    //TODO use when we will finally have metrics which can be used in local context
    /*public void computeMetrics(DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {

        double averageDegree = (graph.vertexSet().stream().map(v -> graph.inDegreeOf(v) + graph.outDegreeOf(v)).reduce(0, (acc, x) -> acc + x)) / graph.vertexSet().size();

        for (DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> g : pickInterestingSubgraphs(graph)) {
            //TODO what metrics do we want for triples?
        }
    }

    private Set<DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>>> pickInterestingSubgraphs(DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graph) {
        Set<DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>>> ans = new HashSet<>();
        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> graphWithoutDatatype = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
        graph.vertexSet().stream().filter(v -> v.getType() != VertexType.PROPERTY).forEach(graphWithoutDatatype::addVertex);
        graph.edgeSet().stream().filter(e -> e.getType() != RelationshipEdgeEnum.PROPERTY).forEach(e -> graphWithoutDatatype.addEdge(e.getFrom(), e.getTo(), e));
        for (Vertex<String> v : graphWithoutDatatype.vertexSet()) {
            for (RelationshipEdge<Vertex<String>> e1 : graphWithoutDatatype.outgoingEdgesOf(v)) {
                Vertex<String> v2 = e1.getTo();
                for (RelationshipEdge<Vertex<String>> e2 : graphWithoutDatatype.outgoingEdgesOf(v2)) {
                    Vertex<String> v3 = e2.getTo();
                    for (RelationshipEdge<Vertex<String>> e3 : graphWithoutDatatype.outgoingEdgesOf(v3)) {
                        DirectedGraph<Vertex<String>, RelationshipEdge<Vertex<String>>> tmp = new SimpleDirectedGraph<>(RelationshipEdgeFactory.INSTANCE);
                        tmp.addVertex(v);
                        tmp.addVertex(v2);
                        tmp.addVertex(v3);
                        tmp.addVertex(e3.getTo());
                        tmp.addEdge(v, v2, e1);
                        tmp.addEdge(v2, v3, e2);
                        tmp.addEdge(v3, e3.getTo(), e3);
                        ans.add(tmp);
                    }
                }
            }
        }
        return ans;
    }*/

    /*
   1. Get all the shortest paths in graph.
   2. Find the longest of the shortest paths for each graph.
   3. The shortest of the longest of the shortest is the radius. [we need to go deeper]
    */
    // FIXME: null check & -1 check są potrzebne, bo wygenerowany graf jest niespójny
    private static <T> List<Integer> getEccentricities(FloydWarshallShortestPaths<Vertex<T>, RelationshipEdge<Vertex<T>>> floydWarshallShortestPaths,
                                                       DirectedGraph<Vertex<T>, RelationshipEdge<Vertex<T>>> graph) {
         /*new ArrayList<>(vertexList.stream()
                .map(from -> vertexList.stream()
                        .filter(x -> !x.equals(from))
                        .filter(x -> floydWarshallShortestPaths.getShortestPath(from, x) != null)
                        .map(x -> floydWarshallShortestPaths.getShortestPath(from, x).getLength())
                        .max(Integer::compare)
                        .get())
                .collect(Collectors.toList()));*/
        List<Vertex<T>> vertexList = Collections.unmodifiableList(new ArrayList<>(graph.vertexSet()));
        List<Integer> eccentricities = new ArrayList<>();
        for (Vertex<T> from : vertexList) {
            int distance = -1;
            for (Vertex<T> to : vertexList) {
                GraphPath<Vertex<T>, RelationshipEdge<Vertex<T>>> shortestPath = floydWarshallShortestPaths.getShortestPath(from, to);
                if (shortestPath != null && shortestPath.getLength() > distance) {
                    distance = floydWarshallShortestPaths.getShortestPath(from, to).getLength();
                }
            }
            if (distance > -1) {
                eccentricities.add(distance);
            }
        }
        return eccentricities;
    }
}
