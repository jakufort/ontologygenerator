package pl.edu.agh.ontologyGenerator.metrics;

/**
 *
 * Created by Jakub Fortunka on 12.02.2017.
 */
public enum MetricsEnum {

    ITERATION_LIMIT("iterations", "Max number of graph generation retries [Integer]", false) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Integer.valueOf(stringValue);
        }
    },
    SIZE("size", "Number of edges [Integer]", false) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Integer.valueOf(stringValue);
        }
    },
    AVG_DEGREE("avgDegree", "Average degree of vertex [Double]", true) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Double.valueOf(stringValue);
        }
    },
    ORDER("order", "Number of vertices [Integer]", false) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Integer.valueOf(stringValue);
        }
    },
    DIAMETER("diameter", "Graph diameter [Double]", true) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Double.valueOf(stringValue);
        }
    },
    RADIUS("radius", "Graph radius [Integer]", true) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Integer.valueOf(stringValue);
        }
    },
    ASSORTATIVITY("assortativity", "Graph assortativity [Double]", true) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Double.valueOf(stringValue);
        }
    },
    LOCAL_CLUSTERING_COEFFICIENT("localClustering", "Graph local clustering coefficient [Double]", true) {
        @Override
        public Number parseMetricValue(String stringValue) {
            return Double.valueOf(stringValue);
        }
    };

    private final boolean displayable;
    private String key;
    private String description;

    MetricsEnum(String key, String description, boolean displayable) {
        this.key = key;
        this.description = description;
        this.displayable = displayable;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDisplayable() {
        return displayable;
    }

    public abstract Number parseMetricValue(String stringValue);
}
