package pl.edu.agh.ontologyGenerator.metrics;

import org.jgrapht.alg.util.Pair;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by Jakub Fortunka on 12.02.2017.
 */
public class UserMetrics {

    private EnumMap<MetricsEnum, Number> metrics = new EnumMap<>(MetricsEnum.class);

    public UserMetrics(Properties properties) {
        this.metrics = processProperties(properties);
    }

    public EnumMap<MetricsEnum, Number> getMetrics() {
        return metrics;
    }

    private EnumMap<MetricsEnum, Number> processProperties(Properties properties) {
        return Arrays.stream(MetricsEnum.values())
                .map(metric -> new Pair<>(metric, properties.getProperty(metric.getKey())))
                .filter(x -> x.second != null)
                .map(x -> new Pair<>(x.first, x.first.parseMetricValue(x.second)))
                .collect(Collectors.toMap(
                        x -> x.first,
                        x -> x.second,
                        (l,r) -> l,
                        () -> new EnumMap<>(MetricsEnum.class)));
    }

    //TODO compare metrics in context of desired graph metrics
    public boolean fstGraphIsBetterThanSecond(GraphMetrics fstGraphMetrics, GraphMetrics sndGraphMetrics) {
        return metrics.entrySet().stream()
                .map(entry -> {
                    double desiredValue = entry.getValue().doubleValue();
                    double fstContenderValue = fstGraphMetrics.getMetric(entry.getKey()).doubleValue();
                    double sndContenderValue = sndGraphMetrics.getMetric(entry.getKey()).doubleValue();
                    return (valuesCorrelation(desiredValue, fstContenderValue) - valuesCorrelation(desiredValue, sndContenderValue))/desiredValue;
                }).reduce(0d, (x, y) -> x+y) < 0;
    }

    private double valuesCorrelation(double desiredValue, double contenderValue) {
        return Math.abs(desiredValue - contenderValue);
    }
}
