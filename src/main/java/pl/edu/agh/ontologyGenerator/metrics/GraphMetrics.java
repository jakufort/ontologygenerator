package pl.edu.agh.ontologyGenerator.metrics;

import org.jgrapht.alg.FloydWarshallShortestPaths;
import pl.edu.agh.ontologyGenerator.model.RelationshipEdge;
import pl.edu.agh.ontologyGenerator.model.Vertex;

import java.util.EnumMap;

/**
 * Created by Jakub Fortunka on 12.02.2017.
 */
public class GraphMetrics<T> {

    private FloydWarshallShortestPaths<Vertex<T>, RelationshipEdge<Vertex<T>>> shortestPaths;

    private EnumMap<MetricsEnum, Number> graphMetrics = new EnumMap<>(MetricsEnum.class);

    public void addMetric(MetricsEnum metric, Number value) {
        graphMetrics.put(metric, value);
    }

    public Number getMetric(MetricsEnum metric) {
        return graphMetrics.get(metric);
    }

    @Override
    public String toString() {
        return graphMetrics.entrySet().stream()
                .filter(x -> x.getKey().isDisplayable())
                .map(x -> x.getKey().toString() + ": " + x.getValue().toString())
                .reduce("", (x, y) -> x + "\n" + y);
    }

    public void setShortestPaths(FloydWarshallShortestPaths<Vertex<T>,RelationshipEdge<Vertex<T>>> shortestPaths) {
        this.shortestPaths = shortestPaths;
    }

    public FloydWarshallShortestPaths<Vertex<T>, RelationshipEdge<Vertex<T>>> getShortestPaths() {
        return shortestPaths;
    }
}
