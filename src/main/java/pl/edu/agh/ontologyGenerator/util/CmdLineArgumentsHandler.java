package pl.edu.agh.ontologyGenerator.util;

import org.apache.commons.cli.*;
import pl.edu.agh.ontologyGenerator.metrics.MetricsEnum;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;

/**
 * Created by Wiktor on 2017-02-14.
 */
public class CmdLineArgumentsHandler {
    final static private CommandLineParser parser = new DefaultParser();
    final static private HelpFormatter formatter = new HelpFormatter();
    private static final String HELP_OPTION = "help";
    private static final String CONFIG_FILE_OPTION = "config";
    private static final Options options = new Options();

    static {
        options.addOption(HELP_OPTION, false, "Provides usage information");
        options.addOption(CONFIG_FILE_OPTION, true, "Path to properties file. Overrides CLI properties!");
        Arrays.stream(MetricsEnum.values())
                .forEach(metric -> options.addOption(metric.getKey(), true, metric.getDescription()));
    }

    public static Optional<Properties> handleCommandLineArguments(String[] args) throws IOException, ParseException {
        CommandLine cli = parser.parse(options, args);

        if (cli.hasOption(HELP_OPTION)) {
            showHelp();
            System.exit(0);
        }

        if (cli.hasOption(CONFIG_FILE_OPTION)) {
            System.out.println("Reading properties from file: " + cli.getOptionValue(CONFIG_FILE_OPTION));
            return getOptionByPropertiesContent(PropertiesUtil.loadExternalProperties(cli.getOptionValue(CONFIG_FILE_OPTION)),
                    "No valid properties found in provided file. Defaulting to graph.properties file.", "Found valid properties in provided file.");
        }

        final Properties properties = new Properties();
        Arrays.stream(MetricsEnum.values())
                .filter(metric -> cli.hasOption(metric.getKey()))
                .forEach(metric -> properties.setProperty(metric.getKey(), cli.getOptionValue(metric.getKey())));
        return getOptionByPropertiesContent(properties, "No valid cli properties found. Defaulting to graph.properties file.",
                "Found valid properties within CLI arguments.");
    }

    public static void showHelp() {
        formatter.printHelp("OntologyGenerator\nDefault properties file is named graph.properties (it should be bundled with this jar)", options);
    }

    private static Optional<Properties> getOptionByPropertiesContent(Properties properties, String errorMessage, String successMessage) {
        if (properties.isEmpty() || !containsValidProperties(properties)) {
            System.out.println(errorMessage);
            return Optional.empty();
        } else {
            System.out.println(successMessage);
            return Optional.of(properties);
        }
    }

    private static boolean containsValidProperties(Properties properties) {
        return Arrays.stream(MetricsEnum.values())
                .anyMatch(metric -> properties.getProperty(metric.getKey()) != null);
    }

}
