package pl.edu.agh.ontologyGenerator.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Wiktor on 2017-02-11.
 */
public class PropertiesUtil {

    public static Properties loadExternalProperties(String filePath) throws IOException {
        return readPropertiesFromStream(new FileInputStream(filePath));
    }

    private static Properties readPropertiesFromStream(InputStream input) throws IOException {
        if (input == null) {
            throw new FileNotFoundException("Wrong path to properties file, mate.");
        }
        Properties properties = new Properties();
        properties.load(input);
        input.close();
        return properties;
    }
}

